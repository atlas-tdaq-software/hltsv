# HLTSV - HLT Supervisor

## tdaq-10-00-00

If at run stop no DCMs are available (e.g. if the whole HLTSV farm
crashed), the HLT supervisor will now time out in the RoIBStopped
transition and continue the shutdown process. The remaining events
in the HLTSV memory are lost.

The timeout in seconds is specified in 
HLTSVApplication.StopTimeout. It is 3 minutes by default and 
should be adjusted to the expected time it normally takes the HLT
farm to process the remaining events after the L1 trigger was
stopped.

## tdaq-09-00-00

### Changing HLT Prescales in a Pre-loaded Partition

The HLTSV now supports pre-scale changes in pre-loaded
data mode. This should make it easier to test this
functionality without the CTP. This is for HLT experts only.

The functionality is enabled when the `TriggerConfiguration.TriggerCoolConnection` 
attribute (a string) is not empty. This should be either a COOL alias 
(for the typical use at Point 1 with the CTP), or a test database, e.g. sqlite.
Never use this with a production database for a test !

1. Initialize the sqlite database

```shell
  % TestHltPsk2CoolWriting  hltPrescaleCool.db
  3
  q
```

2. Set the TriggerCoolConnection to a string like the following (adapting
   the path):

```
    <attr name="TriggerCoolConnection" type="string" val="sqlite://;schema=/scratch/work/rhauser/cmtest/cool/hltPrescaleCool.db;dbname=CONDBR2"/>
```

3. Start your partition

```bash
   % trg_command_trigger -p <PARTITON_NAME> -n HLTSV -c SETHLTPRESCALES --arguments 200
```

4. Check the log output of the HLTSV, there should be some informal lines, otherwise check the error logfile.

```shell
   % TestHltPsk2CoolWriting  hltPrescaleCool.db
   1
   0
   q
```

You should now see the new entries that you set via the `trg_command_trigger` utility.
