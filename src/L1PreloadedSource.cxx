/* SPDX-License-Identifier: Apache-2.0 */

#include <stdlib.h>                               // for getenv, size_t
#include <string.h>                               // for memcpy
#include <unistd.h>                               // for access, usleep, R_OK
#include <algorithm>                              // for find
#include <atomic>                                 // for atomic, __atomic_base
#include <cstdint>                                // for uint32_t
#include <iostream>                               // for operator<<, ostring...
#include <map>                                    // for map, map<>::iterator
#include <memory>                                 // for default_delete, sha...
#include <mutex>                                  // for mutex, lock_guard
#include <string>                                 // for string, allocator
#include <unordered_map>                          // for operator==
#include <utility>                                // for pair
#include <vector>                                 // for vector, vector<>::i...
#include <ctime>                                  // for nanosleep

#include "CTPfragment/CTPdataformat.h"            // for LumiBlockMask, Lumi...
#include "CTPfragment/CTPfragment.h"              // for ctpFormatVersion
#include "DFdal/RoIBPlugin.h"                     // for RoIBPlugin
#include "DFdal/RoIBPluginPreload.h"              // for RoIBPluginPreload
#include "HLTPUDal/HLTImplementationDB.h"
#include "EventStorage/DRError.h"                 // for DRError, DRNOOK
#include "EventStorage/DataReader.h"              // for DataReader
#include "EventStorage/pickDataReader.h"          // for pickDataReader
#include "Issues.h"                               // for FileFailed, ConfigF...
#include "L1Source.h"                             // for L1Source, MAXLVL1RODS
#include "LVL1Result.h"                           // for LVL1Result
#include "TrigConfCool/PrescaleKey2CoolWriter.h"  // for PrescaleKey2CoolWriter
#include "TriggerCommander/HoldTriggerInfo.h"
#include "config/Configuration.h"                 // for Configuration
#include "config/DalObject.h"                     // for Configuration::cast
#include "config/Errors.h"                        // for NotFound
#include "config/map.h"                           // for map
#include "dal/Partition.h"                        // for Partition
#include "dal/TriggerConfiguration.h"             // for TriggerConfiguration
#include "dal/TriggerDBConnection.h"
#include "dal/L1TriggerConfiguration.h"
#include "eformat/FullEventFragment.h"            // for FullEventFragment
#include "eformat/ROBFragment.h"                  // for ROBFragment
#include "eformat/util.h"                         // for get_robs
#include "eformat/write/ROBFragment.h"            // for ROBFragment
#include "eformat/write/node.h"                   // for copy, node_t
#include "ers/LocalContext.h"                     // for ERS_HERE
#include "ers/ers.h"                              // for error, warning, ERS...

namespace hltsv {

    /**
     * \brief The L1PreloadedSource class encapsulates the version of the
     * L1Source class which provides a LVL1Result object from data
     * preloaded from a byte stream formatted file as indicated in
     * the configuration object.
     *
     */
    class L1PreloadedSource : public L1Source {
    public:
        explicit L1PreloadedSource(const std::vector<std::string>& file_names,
                                   const std::string& database = "",
                                   uint32_t smk = 0,
                                   uint32_t l1_psk = 0,
                                   uint32_t hlt_psk = 0);
        ~L1PreloadedSource();

        virtual LVL1Result* getResult() override;
        virtual void        reset(uint32_t run_number) override;
        virtual void        preset() override;

        // Master Trigger interface
        daq::trigger::HoldTriggerInfo hold(const std::string& mask) override;
        void                          setPrescales(uint32_t l1p, uint32_t hltp) override;
        void                          setHLTPrescales(uint32_t hltp) override;

    private:
        unsigned int  m_l1id;
        unsigned int  m_max_l1id;

        std::map<unsigned int, std::vector<uint32_t*>*> m_data;

        unsigned int  m_firstid;
        bool          m_modid;

        std::mutex    m_mutex;

        std::vector<std::string> m_file_names;
        uint32_t                 m_smk;
        std::unique_ptr<TrigConf::PrescaleKey2CoolWriter> m_cool_writer;
    };
}

extern "C" hltsv::L1Source *create_source(Configuration *config, const daq::df::RoIBPlugin *roib, const std::vector<std::string>& file_names)
{
    const daq::df::RoIBPluginPreload *my_config = config->cast<daq::df::RoIBPluginPreload>(roib);
    if(my_config == nullptr) {
        throw hltsv::ConfigFailed(ERS_HERE, "Invalid type for configuration to L1PreloadedSource");
    }
    const daq::core::TriggerConfiguration *trig_config = config->get<daq::core::Partition>(getenv("TDAQ_PARTITION"))->get_TriggerConfiguration();

    uint32_t smk = 0;
    uint32_t l1_psk = 0;
    uint32_t hlt_psk = 0;

    if(trig_config != nullptr) {

        if(auto hlt = trig_config->get_hlt()) {
            if(auto hlt_db = config->cast<daq::hlt::HLTImplementationDB>(hlt)) {
                hlt_psk = (uint32_t) std::stoul(hlt_db->get_hltPrescaleKey());
            }
        }

        if(auto dbconn = trig_config->get_TriggerDBConnection()) {
            smk = dbconn->get_SuperMasterKey();
        }

        if(auto l1 = trig_config->get_l1()) {
            l1_psk = l1->get_Lvl1PrescaleKey();
        }
    }

    return new hltsv::L1PreloadedSource(file_names,
                                        trig_config == nullptr ? "" : trig_config->get_TriggerCoolConnection(),
                                        smk,
                                        l1_psk,
                                        hlt_psk);
}

namespace hltsv {

    const unsigned int MAXUINT = 0XFFFFFFFF;

    L1PreloadedSource::L1PreloadedSource(const std::vector<std::string>& file_names, const std::string& database, uint32_t smk, uint32_t l1_psk, uint32_t hlt_psk)
        : m_l1id(0),
          m_file_names(file_names),
          m_smk(smk),
          m_cool_writer(database.empty() ? nullptr : new TrigConf::PrescaleKey2CoolWriter(database))
    {
        if(m_file_names.empty()) {
            throw hltsv::ConfigFailed(ERS_HERE, "Empty list of file names for L1PreloadedSource");
        }
        setLumiBlockInterval(60); // default value in L1Source is 120 seconds
        m_l1_prescale  = l1_psk;
        m_hlt_prescale = hlt_psk;

        if(m_cool_writer) {
            m_cool_writer->publishSmk2IS(m_smk, "From preloaded HLTSV");
            m_cool_writer->publishL1Psk2IS(m_l1_prescale, "From preloaded HLTSV");
            m_cool_writer->publishHltPsk2IS(m_hlt_prescale, "From preloaded HLTSV");
        }
    }

    L1PreloadedSource::~L1PreloadedSource()
    {
        // cleanup
        std::map<unsigned int, std::vector<uint32_t*>*>::iterator mit;

        for ( mit=m_data.begin(); mit!=m_data.end(); mit++ ) {
            std::vector<uint32_t*>* rob_frag = (*mit).second;
            std::vector<uint32_t*>::iterator fit;
            for ( fit=rob_frag->begin(); fit!=rob_frag->end(); fit++ ) {
                delete [] (*fit);
            }
            delete (*mit).second;
        }
    }

    LVL1Result* L1PreloadedSource::getResult()
    {

        if(m_hold) return nullptr;

        // Create LVL1result, re-use ROBFragment
        LVL1Result* l1Result = 0;

        uint32_t* robs[MAXLVL1RODS];
        uint32_t  lengths[MAXLVL1RODS];

        std::vector<uint32_t*>* rob_frag = m_data[m_l1id];

        std::vector<uint32_t*>::iterator fit = rob_frag->begin();

        uint32_t lvl1_id = 0;

        for ( size_t i=0; i< rob_frag->size(); i++, fit++ ) {
            /**
             * Update Lumi Block Number in Rob's header
             */
            uint32_t event_type = 1; /// ??? params->getLumiBlock();
            // --- copy a writable ROB wrob
            eformat::write::ROBFragment wrob = *fit;
            // --- set event_type
            wrob.rod_detev_type(event_type);

            // --- set event type with Lumi Block from L2SVP
            if ( m_max_l1id > 0 && m_modid ) {
                // eformat::write::ROBFragment test_frag(const_cast<uint32_t*>(robs[i]));
                uint32_t old_l1id = wrob.rod_lvl1_id();
                // if ( (MAXUINT - test_frag.rod_lvl1_id()) > m_max_l1id )
                if ( (MAXUINT - old_l1id) > m_max_l1id ) {
                    // test_frag.rod_lvl1_id( test_frag.rod_lvl1_id() + m_max_l1id );
                    wrob.rod_lvl1_id(old_l1id + m_max_l1id);
                } else {
                    // test_frag.rod_lvl1_id( test_frag.rod_lvl1_id() % m_max_l1id );
                    wrob.rod_lvl1_id(old_l1id % m_max_l1id);
                }
            }
            wrob.rod_run_no(m_run_number);
            // --- bind and copy fragment back
            const eformat::write::node_t* toplist = wrob.bind();
            robs[i] = new uint32_t[wrob.size_word()];
            lengths[i] = wrob.size_word();
            eformat::write::copy(*toplist, robs[i], wrob.size_word());

            uint32_t* rob = robs[i];
            // Rewrite LB and HLT counter in CTP fragment
            const eformat::ROBFragment<const uint32_t*> rob_frag(const_cast<uint32_t*>(robs[i]));

            std::lock_guard<std::mutex> lock(m_mutex);

            if ( rob_frag.source_id()==0x00770001 ) {    // CTP fragment
                if ( CTPfragment::ctpFormatVersion(&rob_frag) == 0 ) {
                    // test_frag.rod_detev_type(...)
                    //
                    //  read lb once atomically...
                    uint32_t lb = m_lb;
                    rob[16] = ((lb & CTPdataformat::LumiBlockMask) << CTPdataformat::LumiBlockShift) |
                        ((m_hlt_counter & CTPdataformat::HltCounterMask_v0) << CTPdataformat::HltCounterShift_v0);
                }
                else {
                    uint32_t lb = m_lb;
                    rob[16] = ((lb & CTPdataformat::LumiBlockMask) << CTPdataformat::LumiBlockShift) |
                        ((m_hlt_counter & CTPdataformat::HltCounterMask_v1) << CTPdataformat::HltCounterShift_v1);
                    if ( CTPfragment::ctpFormatVersion(&rob_frag) >= 2 ) {
                        // copied/hacked from CTPfragment::extraPayloadWords(...)
                        const uint32_t *rod;
                        rob_frag.rod_start(rod);
                        uint32_t *rod_write = const_cast<uint32_t*>(rod);

                        // Update time stamp to current time
                        timespec now;
                        clock_gettime(CLOCK_REALTIME, &now);
                        rod_write[rod[CTPdataformat::Helper::HeaderSizePos] + CTPdataformat::TimeSecondsPos]     = now.tv_sec;
                        rod_write[rod[CTPdataformat::Helper::HeaderSizePos] + CTPdataformat::TimeNanosecondsPos] = ((now.tv_nsec/CTPdataformat::TimeNanosecondsTicks) << CTPdataformat::TimeNanosecondsOffset) & ~0xF;

                        uint32_t numberExtraWords =  CTPfragment::numberExtraPayloadWords(rod);
                        // startIndex is the fragment size, hence points to one after the
                        // final word
                        uint32_t startIndex = rob_frag.rod_fragment_size_word();
                        // correct for trailer
                        startIndex -= rob_frag.rod_trailer_size_word();
                        // correct for status words
                        startIndex -= rob_frag.rod_nstatus();
                        // now subtract the additional words
                        startIndex -= numberExtraWords;
                        // startIndex is now exactly the size of the trigger-bit part of
                        // the payload data. hence it points to the first of the extra
                        // words, which is the time since the previous crossing; to get to
                        // the true extra payload words for the HLT, add 2 to account for
                        // the time-since-previous-crossing word, and the new turn counter
                        // (in case format version number is at least 3)
                        startIndex += 1;
                        if( CTPfragment::ctpFormatVersion(&rob_frag) >= 3) {
                            startIndex += 1;
                        }

                        // Now we point at the BG and L1PSK, go one further
                        startIndex += 1;

                        uint32_t updateCount = rod_write[startIndex];
                        uint32_t  update_lb = m_folders[2];

                        bool found = false;
                        for(uint32_t i = 1; i <= updateCount ; i++) {
                            // Check if there is already an HLT PSK update, if yes use it.
                            if ((rod_write[startIndex + updateCount] & 0xffff) == 2) {
                                rod_write[startIndex + updateCount] = (update_lb << 16) | (2 & 0xffff);
                                found = true;
                            }
                        }

                        if(!found) {
                            rod_write[startIndex] = 1;
                            rod_write[startIndex + 1] = (update_lb << 16) | (2 & 0xffff);
                        }
                    }
                }
            }

            if(i == 0) {
                lvl1_id = wrob.rod_lvl1_id();
            }

            // mutex is unlocked here
        }

        if (rob_frag->size()) {
            l1Result = new LVL1Result( lvl1_id, rob_frag->size(), robs, lengths);
        } else {
            std::ostringstream mesg;
            mesg <<"looking for LVL1 RoIs to build LVL1Result with l1id " <<
                m_l1id ;
            FileFailed issue(ERS_HERE,(mesg.str()).c_str());
            ers::warning(issue);
        }
        m_l1id += 1;
        if (m_data.size() <= m_l1id) {
            m_l1id = m_firstid;
            m_modid = true;
        }

        return l1Result;
    }

    void L1PreloadedSource::preset()
    {
        // clean any stored fragments
        std::map<unsigned int, std::vector<uint32_t*>*>::iterator mit;
        for ( mit=m_data.begin(); mit!=m_data.end(); mit++ ) {
            std::vector<uint32_t*>* rob_frag = (*mit).second;
            std::vector<uint32_t*>::iterator fit;
            for ( fit=rob_frag->begin(); fit!=rob_frag->end(); fit++ ) {
                delete [] (*fit);
            }
            delete (*mit).second;
        }

        // also reset l1_id index
        m_l1id = 0;
        m_max_l1id = 0;
        m_run_number = 0;
        m_modid = false;

        // empty the event map
        m_data.erase(m_data.begin(), m_data.end());

        std::string file = m_file_names[0] + ".per-rob";

        if ((access(file.c_str(), R_OK | X_OK)) == -1) {
            for (std::vector<std::string>::const_iterator it = m_file_names.begin();
                 it != m_file_names.end(); it++) {
                //Open the file and read-in event per event
                DataReader *pDR = pickDataReader(*it);

                if(!pDR) {
                    std::ostringstream mesg;
                    mesg<<"opening data file " << *it;
                    hltsv::FileFailed issue(ERS_HERE,(mesg.str()).c_str());
                    ers::error(issue);
                    break;
                }

                if(!pDR->good()) {
                    std::ostringstream mesg;
                    mesg<<"reading data file " << pDR->fileName();
                    hltsv::FileFailed issue(ERS_HERE,(mesg.str()).c_str());
                    ers::error(issue);
                    break;
                }

                while(pDR->good()) {
                    unsigned int eventSize;
                    char *buf;

                    DRError ecode = pDR->getData(eventSize,&buf);

                    while(DRWAIT == ecode) {// wait for more data to come
                        timespec wait{0, 500000000};
                        nanosleep(&wait, nullptr);
                        ecode = pDR->getData(eventSize,&buf);
                    }

                    if(DRNOOK == ecode) {
                        break;
                    }

                    eformat::FullEventFragment<const uint32_t*>
                        fe(reinterpret_cast<const uint32_t*>(buf));

                    // get ROBFragments
                    std::vector<const uint32_t*> robs(fe.nchildren());
                    size_t nrobs = fe.children(&robs[0], robs.size());

                    m_data[m_l1id] = new std::vector<uint32_t*>;
                    for ( size_t i=0; i<nrobs; i++) {
                        eformat::write::ROBFragment test_frag(const_cast<uint32_t*>(robs[i]));

                        switch (test_frag.source_id()) {
                        case 0x00770001:
                        case 0x00760001:
                        case 0x007500AC:
                        case 0x007500AD:
                        case 0x007300A8:
                        case 0x007300A9:
                        case 0x007300AA:
                        case 0x007300AB:
                        case 0x00910081:
                        case 0x00910091:
                            {
                                uint32_t len = test_frag.size_word();
                                uint32_t*rob_frag = new uint32_t[len];

                                const eformat::write::node_t* list = test_frag.bind();
                                len = eformat::write::copy( list[0],
                                                            rob_frag,
                                                            len);

                                m_data[m_l1id]->push_back(rob_frag);

                                if (test_frag.rod_lvl1_id() > m_max_l1id)
                                    m_max_l1id = test_frag.rod_lvl1_id();
                            }
                            break;

                        default:
                            break;
                        }
                    }
                    m_l1id++;
                    delete[] buf;
                }
                delete pDR;
            }
        } else {

            std::vector<std::string> l1_frag{
                "0x00770001",
                "0x00760001",
                "0x007500AC",
                "0x007500AD",
                "0x007300A8",
                "0x007300A9",
                "0x007300AA",
                "0x007300AB",
                "0x00910081",
                "0x00910091"
            };

            std::vector<std::string> l1_frag_ignore{"0x00910081","0x00910091"};

            // Read through data files, looking for LVL1 ROBFragments
            for ( size_t fit=0; fit<l1_frag.size(); fit++) {
                m_l1id=0;
                for (std::vector<std::string>::const_iterator it = m_file_names.begin();
                     it != m_file_names.end(); it++) {
                    std::string fileName = (*it) + ".per-rob/" + l1_frag[fit] + ".data";

                    if(access(fileName.c_str(), R_OK) == -1) {
                        if(std::find(l1_frag_ignore.begin(), l1_frag_ignore.end(), l1_frag[fit]) != l1_frag_ignore.end()) {
                            continue;
                        }
                    }

                    //Open the file and read-in event per event
                    DataReader *pDR = pickDataReader(fileName);
                    ERS_DEBUG(2, "Loading data file " << fileName);

                    if(!pDR) {
                        std::ostringstream mesg;
                        mesg<<"opening data file " << fileName;
                        hltsv::FileFailed issue(ERS_HERE,(mesg.str()).c_str());
                        ers::error(issue);
                        break;
                    }

                    if(!pDR->good()) break;

                    while(pDR->good()) {
                        unsigned int eventSize;
                        char *buf;

                        DRError ecode = pDR->getData(eventSize,&buf);

                        while(DRWAIT == ecode) { // wait for more data to come
                            timespec wait{0, 500000000};
                            nanosleep(&wait, nullptr);
                            ecode = pDR->getData(eventSize,&buf);
                        }

                        if(DRNOOK == ecode) break;

                        std::vector<std::shared_ptr<const uint32_t>> robs;
                        eformat::get_robs(reinterpret_cast<const uint32_t*>(buf), robs);
                        size_t nrobs = robs.size();

                        if (nrobs > 0) {

                            if (fit == 0) m_data[m_l1id] = new std::vector<uint32_t*>;
                            for ( size_t i=0; i<nrobs; i++) {
                                eformat::write::ROBFragment test_frag(const_cast<uint32_t*>(robs[i].get()));

                                uint32_t len = test_frag.size_word();
                                uint32_t*rob_frag = new uint32_t[len];

                                memcpy(rob_frag, robs[i].get(), len * sizeof(uint32_t));
                                m_data[m_l1id]->push_back(rob_frag);

                                if (test_frag.rod_lvl1_id() > m_max_l1id) {
                                    m_max_l1id = test_frag.rod_lvl1_id();
                                }
                            }
                            m_l1id++;
                        }
                        delete[] buf;
                    }
                    delete pDR;
                }
            }
        }

        if (m_data.empty()) {
            hltsv::FileFailed issue0(ERS_HERE, "looking for valid L1 results" );
            ers::error(issue0);
            hltsv::NoL1ResultsException issue(ERS_HERE, issue0);
            throw issue;
        }

        // only preloadedMod...
        m_max_l1id++;
    }

    void L1PreloadedSource::reset(uint32_t run_number)
    {
        m_firstid = m_l1id = 0;
        m_run_number = run_number;
        m_lb = 1;
        m_hlt_counter = 1;
        m_hold = 1;

        if(m_cool_writer) {
            m_cool_writer->writeHLTPsk2Cool(m_run_number, 0 , m_hlt_prescale);
            m_cool_writer->publishSmk2IS(m_smk, "From preloaded HLTSV");
            m_cool_writer->publishL1Psk2IS(m_l1_prescale, "From preloaded HLTSV");
            m_cool_writer->publishHltPsk2IS(m_hlt_prescale, "From preloaded HLTSV");
        }
        m_folders[2] = m_lb.load();
    }

    daq::trigger::HoldTriggerInfo L1PreloadedSource::hold(const std::string& dm)
    {
        L1Source::hold(dm);
        return {(uint8_t)((m_l1id >> 24) & 0xff), m_l1id & 0xffffff};
    }

    // Master Trigger interface
    void L1PreloadedSource::setPrescales(uint32_t l1p, uint32_t hltp)
    {
        bool save_update = m_update;
        m_update = false;
        if(m_cool_writer) {
            if(!m_cool_writer->writeHLTPsk2Cool(m_run_number, m_lb + 1, hltp)) {
                return;
            }
            m_cool_writer->publishL1Psk2IS(l1p, "From preloaded HLTSV");
            m_cool_writer->publishHltPsk2IS(hltp, "From preloaded HLTSV");
        }
        m_folders[2] = m_lb + 1;
        L1Source::setPrescales(l1p, hltp);
        m_update = save_update;
    }

    void L1PreloadedSource::setHLTPrescales(uint32_t hltp)
    {
        bool save_update = m_update;
        m_update = save_update;
        if(m_cool_writer) {
            if(!m_cool_writer->writeHLTPsk2Cool(m_run_number, m_lb + 1, hltp)) {
                return;
            }
            m_cool_writer->publishHltPsk2IS(hltp, "From preloaded HLTSV");
        }
        m_folders[2] = m_lb + 1;
        L1Source::setHLTPrescales(hltp);
        m_update = save_update;
    }

}
