/* SPDX-License-Identifier: Apache-2.0 */

#include "MulticastROSClear.h"
#include "ClearMessage.h"
#include "ers/ers.h"

#include <boost/asio/basic_deadline_timer.hpp>                 // for basic_...
#include <boost/asio/error.hpp>                                // for basic_...
#include <boost/asio/io_context.hpp>                           // for io_con...
#include <boost/asio/ip/address.hpp>                           // for address
#include <boost/asio/ip/udp.hpp>                               // for udp
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/system/error_code.hpp>                         // for error_...

#include "asyncmsg/Message.h"                                  // for Output...
#include "asyncmsg/NameService.h"                              // for NameSe...
#include "asyncmsg/UDPSession.h"                               // for UDPSes...
#include "asyncmsg/detail/Header.h"                            // for asyncmsg
#include "ers/Assertion.h"                                     // for ERS_AS...
#include "ers/Issue.h"                                         // for ERS_IS...
#include "src/ROSClear.h"                                      // for ROSClear

#include <boost/asio/basic_deadline_timer.hpp>                 // for basic_...
#include <boost/asio/error.hpp>                                // for basic_...
#include <boost/asio/io_context.hpp>                           // for io_con...
#include <boost/asio/ip/address.hpp>                           // for address
#include <boost/asio/ip/udp.hpp>                               // for udp
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/system/error_code.hpp>                         // for error_...

#include <cstdint>                                             // for uint32_t
#include <ctime>                                               // for nanosleep
#include <functional>                                          // for _Bind_...
#include <utility>                                             // for move

namespace boost { namespace asio { class const_buffer; } }


namespace hltsv {

    class KeepAliveMessage : public daq::asyncmsg::OutputMessage {
    public:

        static const uint32_t ID = 0x00DCDF11;

         KeepAliveMessage() {};
        ~KeepAliveMessage() {};
        virtual uint32_t typeId() const override { return ID;};
        virtual uint32_t transactionId() const override {return 0;};
        virtual void     toBuffers(std::vector<boost::asio::const_buffer>&) const override {};
    };

    class MCSession : public daq::asyncmsg::UDPSession {
    public:
        explicit MCSession(boost::asio::io_context& service)
            : daq::asyncmsg::UDPSession(service)
        {
        }

    protected:
        std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(uint32_t, uint32_t, uint32_t) noexcept override
        {
            ERS_ASSERT_MSG(false, "Should never happen");
            return std::unique_ptr<daq::asyncmsg::InputMessage>();
        }


        void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage>) noexcept override
        {
            ERS_ASSERT_MSG(false, "Should never happen");
        }

        void onReceiveError(const boost::system::error_code&, std::unique_ptr<daq::asyncmsg::InputMessage>) noexcept override
        {
            ERS_ASSERT_MSG(false, "Should never happen");
        }

        void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>) noexcept override
        {
            // just delete message
        }

        void onSendError(const boost::system::error_code& error, std::unique_ptr<const daq::asyncmsg::OutputMessage>) noexcept override
        {
            ERS_LOG("send error: " << error);
        }

    };

    MulticastROSClear::MulticastROSClear(size_t threshold, boost::asio::io_context& service, const std::string& multicast_address, const std::string& outgoing)
        : ROSClear(threshold),
          m_session(new MCSession(service)),
          m_timer(service)
    {
        m_session->setOutgoingInterface(daq::asyncmsg::NameService::find_interface(outgoing));
        m_session->connect(boost::asio::ip::udp::endpoint(boost::asio::ip::make_address(multicast_address), 9000));
    }

    MulticastROSClear::~MulticastROSClear()
    {
      //stop the keepalive
      m_timer.cancel();
    }

    void MulticastROSClear::connect()
    {
      // Give some time to the ROSs to join the group
        timespec wait{0, 500000000};
        nanosleep(&wait, nullptr);

      // Start the keepalive
      m_timer.expires_from_now(boost::posix_time::seconds(0));
      this->do_ping(boost::system::error_code());
    }

    void MulticastROSClear::do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events)
    {
        using namespace daq::asyncmsg;

        std::unique_ptr<const OutputMessage> msg(new ClearMessage(sequence,events));
        m_session->asyncSend(std::move(msg));
    }

    void MulticastROSClear::do_ping(const boost::system::error_code& e)
    {
      if (e != boost::asio::error::operation_aborted) {
	//send the keepalive
	std::unique_ptr<const daq::asyncmsg::OutputMessage>
	  msg(new KeepAliveMessage());
	m_session->asyncSend(std::move(msg));

	m_timer.expires_at(m_timer.expires_at() + boost::posix_time::seconds(30));
	m_timer.async_wait(std::bind(&MulticastROSClear::do_ping, this,
				    std::placeholders::_1));
      }
    }
}

