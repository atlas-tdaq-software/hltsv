/* SPDX-License-Identifier: Apache-2.0 */

#include "Activity.h"
#include "HLTSV.h"
#include "HLTSVServer.h"
#include "Issues.h"

#include "config/Configuration.h"

#include "dal/Partition.h"
#include "dal/MasterTrigger.h"

#include "DFdal/DFParameters.h"
#include "DFdal/DataFile.h"

#include "rc/RunParamsNamed.h"

#include "L1Source.h"
#include "LVL1Result.h"
#include "ROSClear.h"
#include "UnicastROSClear.h"
#include "MulticastROSClear.h"
#include "EventScheduler.h"

#include "DFdal/HLTSVApplication.h"
#include "DFdal/RoIBPlugin.h"
#include "DFdal/RoIBMasterTriggerPlugin.h"

#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMStates.h"

#include "TriggerCommander/CommandedTrigger.h"
#include "TriggerCommander/MasterTrigger.h"

#include "monsvc/MonitoringService.h"
#include "monsvc/PublishingController.h"

#include "asyncmsg/NameService.h"
#include "is/info.h"

#include "ers/ers.h"

#include <cstdlib>
#include <algorithm>
#include <chrono>
#include <ctime>

namespace hltsv {

    void Activity::Deleter::operator()(daq::trigger::CommandedTrigger *p) { if(p) {p->shutdown(); } }

    Activity::Activity(bool restarted)
        : daq::rc::Controllable(),
          m_l1source(nullptr),
          m_network(false),
          m_running(false),
          m_cmdReceiver(nullptr),
          m_restarted(restarted)
    {
    }

    Activity::~Activity() noexcept
    {
    }

    void Activity::configure(const daq::rc::TransitionCmd& )
    {

        Configuration& conf = daq::rc::OnlineServices::instance().getConfiguration();

        const daq::df::HLTSVApplication* self = conf.cast<daq::df::HLTSVApplication>(&daq::rc::OnlineServices::instance().getApplication());

        const daq::core::Partition& partition = daq::rc::OnlineServices::instance().getPartition();
        const IPCPartition          part(daq::rc::OnlineServices::instance().getIPCPartition());

        const daq::df::DFParameters *dfparams = conf.cast<daq::df::DFParameters>(partition.get_DataFlowParameters());

        m_event_delay  = self->get_EventDelay();
        m_stop_timeout = self->get_StopTimeout();

        // Get list of pre-loaded file names, if any.
        std::vector<std::string> file_names;
        for(auto& df : dfparams->get_UsesDataFiles()) { file_names.emplace_back(df->get_FileName()); }

        // Load L1 Source
        const daq::df::RoIBPlugin *source = self->get_RoIBInput();

        try {
            for(const auto& lib_name : source->get_Libraries()) {
                m_l1source_libs.emplace_back(lib_name + ".so",
                                             boost::dll::load_mode::type::search_system_folders |
                                             boost::dll::load_mode::type::rtld_now);
            }

            if(m_l1source_libs.size() == 0) {
                throw ConfigFailed(ERS_HERE, "No libraries specified for RoIBPlugin");
            }

            auto make =  m_l1source_libs.back().get<L1Source::creator_t>("create_source");
            m_l1source = std::unique_ptr<L1Source>(make(&conf, source, file_names));
            m_l1source->preset();

        } catch (ers::Issue& ex) {
            ers::fatal(ex);
            return;
        } catch (...) {
            ConfigFailed ex(ERS_HERE, "Cannot load RoIB plugin");
            ers::fatal(ex);
            return;
        }

        if(source->cast<daq::df::RoIBMasterTriggerPlugin>()) {
            m_cmdReceiver = std::unique_ptr<daq::trigger::CommandedTrigger, Deleter>(new daq::trigger::CommandedTrigger(part, daq::rc::OnlineServices::instance().applicationName(), m_l1source.get()));
        }

        m_publisher.reset(new monsvc::PublishingController(part,"HLTSV"));
        m_publisher->add_configuration_rules(conf, self);

        m_io_services.reset(new std::vector<boost::asio::io_context>(self->get_NumberOfAssignThreads()));

        m_work.reset(new std::vector<work>());
        for(auto& svc : *m_io_services) {
            (*m_work).emplace_back(svc.get_executor());
        }

        m_ros_work.reset(new work(m_ros_io_service.get_executor()));

        // Initialize  HLTSV_NameService
        std::vector<std::string> data_networks = dfparams->get_DefaultDataNetworks();
        daq::asyncmsg::NameService HLTSV_NameService(part, data_networks);

        // Initialize ROS clear implementation
        if(dfparams->get_MulticastAddress().empty()) {
            // use TCP
            m_ros_clear = std::make_shared<UnicastROSClear>(self->get_ClearGrouping(), m_ros_io_service, HLTSV_NameService);
        } else {

            // address is format  <Multicast-IP-Adress>/<OutgoingInterface>

            auto mc = dfparams->get_MulticastAddress();
            auto n = mc.find('/');
            auto mcast = mc.substr(0, n);
            auto outgoing = mc.substr(n + 1);

            ERS_LOG("Configuring for multicast: " << mcast << '/' << outgoing);

            m_ros_clear = std::make_shared<MulticastROSClear>(self->get_ClearGrouping(), m_ros_io_service, mcast, outgoing);
        }

        m_network = true;

        m_stats = monsvc::MonitoringService::instance().register_object("Events",new HLTSV(), true, std::bind(&Activity::update_monitoring, this, std::placeholders::_1,  std::placeholders::_2));
        m_event_sched = std::make_shared<EventScheduler>(m_stats);
        m_myServer = std::make_shared<HLTSVServer> (*m_io_services, m_event_sched, m_ros_clear, self->get_Timeout(), m_stats);

        m_myServer->listen(daq::rc::OnlineServices::instance().applicationName());

        boost::asio::ip::tcp::endpoint my_endpoint = m_myServer->localEndpoint();

        // Publish port in IS for DCM
        HLTSV_NameService.publish(daq::rc::OnlineServices::instance().applicationName(), my_endpoint.port());

        // restart handling, try to read old IS information and continue from there.
        if(m_restarted) {
            // This throws an exception, e.g. because the HLTSV information is not there
            // we want to stop, since we have no idea with which event ID to recover. So let
            // the IS exception just propagate.
            HLTSV hltsv_info;
            ISInfoDictionary dict(daq::rc::OnlineServices::instance().getIPCPartition());
            dict.getValue("DF.HLTSV.Events",hltsv_info);

            m_initial_event_id = hltsv_info.Recent_Global_ID + 10000000;

            ERS_LOG("HLTSV has been restarted during run; starting with global event ID" << m_initial_event_id);
        } else {
            m_initial_event_id = 0;
        }

        // clear statistics, counters, event queues
        m_event_sched->reset(m_initial_event_id);
        m_stats->reset();

        m_initial_event_id = 0;

        //  HLTSVServer::start() calls Server::asyncAccept() which calls HLTSVServer::onAccept
        m_myServer->start();

        for(unsigned int i = 0; i < self->get_NumberOfAssignThreads(); i++) {
            m_io_threads.push_back(std::thread(&Activity::io_thread, this, std::ref((*m_io_services)[i])));
        }

        m_io_threads.push_back(std::thread(&Activity::io_thread, this, std::ref(m_ros_io_service)));

        return;
    }

    void Activity::connect(const daq::rc::TransitionCmd& )
    {
        // ROS_Clear with TCP will establish the connection
        m_ros_clear->connect();

        m_publisher->start_publishing();
        return;
    }

    void Activity::prepareForRun(const daq::rc::TransitionCmd& )
    {

        m_event_sched->shuffle();

        m_ros_clear->prepareForRun();

        const IPCPartition  partition(daq::rc::OnlineServices::instance().getIPCPartition());

        RunParamsNamed runparams(partition, "RunParams.SOR_RunParams");
        runparams.checkout();

        m_l1source->reset(runparams.run_number);
        m_max_events = runparams.max_events;

#if 0
        // For the FILAR input we need a second reset after
        // a delay to make it *really* work.
        //
        // This is safe for the other plugins as well, since they
        // just reset some counters and clear data structures.
        //
        // Note: April 2020: we don't support the FILAR anymore
        // and this conflicts with the preloaded mode where we have
        // to store data to COOL.
        //
        timespec wait{0,500000000};
        nanosleep(&wait);
        m_l1source->reset(runparams.run_number);
#endif

        if(m_cmdReceiver) {
            m_l1source->startLumiBlockUpdate();
        }

        m_finished = false;
        m_running = true;
        m_l1_thread = std::thread(&Activity::l1_thread, this);

        return;
    }

    void Activity::stopDC(const daq::rc::TransitionCmd& )
    {
        std::unique_lock lock(m_finished_mutex);

        // Stop any further input from L1
        // At this stage L1 should have already stopped sending new data
        // note: RobinNP plugin does not implement this.
        m_l1source->stop();

        // Stop the main event loop (l1_thread())
        m_running = false;

        // If we are the master trigger, stop lumi block updates.
        // This is never our job for real data taking, only for
        // the emulation modes (pre-loaded data).
        if(m_cmdReceiver) {
            m_l1source->stopLumiBlockUpdate();
        }

        // If the l1_thread() loop has already received an input
        // event, but no DCMs are available (e.g. if the whole
        // HLT farm crashed) it will be blocked in schedule_event()
        // and consequently the following join() will also block
        // forever.
        //
        // We wait here StopTimeout seconds, then
        // we call abort() on the DCM queue.
        // This will in turn throw the tbb::user_abort() on
        // the queue::pop() method and unblock the scheduler.
        if(!m_finished_cond.wait_for(lock, std::chrono::seconds(m_stop_timeout), [this] { return m_finished; })) {
            m_event_sched->abort();
        }

        lock.unlock();

        m_l1_thread.join();

        return;
    }

    void Activity::stopRecording(const daq::rc::TransitionCmd& )
    {
        // reset scheduler
        m_event_sched->reset(0);
        m_ros_clear->flush();
    }


    void Activity::unconfigure(const daq::rc::TransitionCmd& )
    {
        m_myServer->stop();

        m_network = false;

        m_work.reset();
        m_ros_work.reset();
        m_event_sched.reset();
        m_ros_clear.reset();
        m_myServer.reset();

        monsvc::MonitoringService::instance().remove_object(std::string("Events"));

        for(auto& thr : m_io_threads) {
            thr.join();
        }
        m_io_threads.clear();

        m_l1source.reset();
        m_l1source_libs.clear();

        for(auto& svc : *m_io_services) {
            svc.restart();
        }
        m_ros_io_service.restart();

        if(m_cmdReceiver) {
            m_cmdReceiver.reset();
        }

        return;
    }

    void Activity::disconnect(const daq::rc::TransitionCmd&  )
    {
        m_publisher->stop_publishing();
        return;
    }

    void Activity::user(const daq::rc::UserCmd& cmd)
    {
        if (cmd.commandName() == "HLTSV_SET_DELAY") {

            if (daq::rc::FSMStates::stringToState(cmd.currentFSMState()) < daq::rc::FSM_STATE::CONFIGURED) {
                ERS_LOG("Received HLTSV_SET_DELAY command, but the application is not yet configured. Ignoring.");
            } else if (cmd.commandParameters().size() != 1) {
                ERS_LOG("Received HLTSV_SET_DELAY command with "
                        << cmd.commandParameters().size() <<
                        " parameters, but one parameter is needed. Ignoring.");
            } else {
                // Parse parameter
                auto param = cmd.commandParameters().front();
                // Parse value
                try {
                    m_event_delay = boost::lexical_cast<unsigned int>(param);
                    ERS_LOG("Received HLTSV_SET_DELAY command with parameter "
                            << cmd.commandParameters().front() <<
                            ": target event delay updated to << "
                            << m_event_delay << ".");
                } catch (boost::bad_lexical_cast& ex) {
                    ERS_LOG("Received HLTSV_SET_DELAY command with parameter "
                            << cmd.commandParameters().front() <<
                            ", but could not parse parameter: " << ex.what()
                            << ". Ignoring.");
                }
            }
        }
    }

    void Activity::onExit(daq::rc::FSM_STATE state) noexcept
    {
        // If we are asked to exit without being in state
        // INITIAL we exit without cleanup. Since the
        // state of threads, locks and other data structures
        // is unknown we can't really cleanly shut down and
        // risk a SIGKILL otherwise.
        if(state > daq::rc::FSM_STATE::INITIAL) {
            std::quick_exit(0);
        }
    }

    void Activity::l1_thread()
    {
        // const std::uint64_t ms = 1000ull;
        // const std::uint64_t s = 1000ull * 1000ull;

        using namespace std::chrono;

        auto period   = microseconds(m_event_delay);
        auto deadline = steady_clock::now() + period;

        uint64_t assigned = 0;   // counter of events from L1Source, to check for max events use case

        while(m_running) {

            try {
                // Get input from Level 1
                std::shared_ptr<LVL1Result> result(m_l1source->getResult());

                if(result) {
                    // Schedule it on a DCM
                    m_event_sched->schedule_event(result);
                    assigned++;

                    if(m_cmdReceiver && m_max_events > 0 && assigned == m_max_events) {
                        m_running = false;
                    }
                }
            } catch (ers::Issue& ex) {
                ers::error(ex);
            } catch (tbb::user_abort& ex) {
                ERS_LOG("tbb::user_abort: " << ex.what());
                // we break explicitly although m_running should be false by this point
                break;
            } catch (std::exception& ex) {
                ERS_LOG("Unexcpected std::exception " << ex.what());
            } catch (...) {
                ERS_LOG("Unknown exception thrown");
            }

            if (m_event_delay != 0) {
                auto now = steady_clock::now();

                // this will update the period if changed via user command
                period = microseconds(m_event_delay);

                if (now > deadline + seconds(1)) {
                    // We overshot the deadline by more than 1 s.
                    // Reset the next deadline
                    // and go immediately back to scheduling
                    deadline = now + period;
                } else {
                    // Busy-wait until the deadline is reached.
                    while (now < deadline) {
                        now = steady_clock::now();
                    }
                    deadline += period;
                }
            }
        }

        // Tell the run control thread that we are finished
        // and it can go on and join us.
        std::unique_lock lock(m_finished_mutex);
        m_finished = true;
        m_finished_cond.notify_one();
    }


    void Activity::io_thread(boost::asio::io_context& service)
    {
        while(m_network) {
            service.run();
        }
    }

    void Activity::update_monitoring(const std::string& /* name */, HLTSV *info)
    {
        if(m_l1source) {
            m_l1source->getMonitoringInfo(info);
        }
    }

}
