/* SPDX-License-Identifier: Apache-2.0 */

#include "LVL1Result.h"
#include "L1Source.h"
#include "Issues.h"

#include "TriggerCommander/HoldTriggerInfo.h"
#include "eformat/SourceIdentifier.h"   // for SourceIdentifier, TDAQ_CTP
#include "eformat/Status.h"             // for STATUS_FRONT
#include "eformat/write/ROBFragment.h"  // for ROBFragment
#include "eformat/write/node.h"         // for copy

#include "config/Configuration.h"
#include "DFdal/RoIBPlugin.h"           // for RoIBPlugin
#include "DFdal/RoIBPluginInternal.h"

#include "CTPfragment/CTPdataformat.h"

#include <atomic>                       // for atomic, __atomic_base
#include <cstdint>                      // for uint32_t
#include <memory>                       // for default_delete
#include <mutex>                        // for lock_guard, mutex
#include <string>                       // for string
#include <vector>                       // for vector


namespace hltsv {
    /**
     * \brief The L1InternalSource class encapsulates the version of the
     * L1Source class which provides a LVL1Result object internally
     * from configuration parameters.
     *
     */
    class L1InternalSource : public L1Source {
    public:
        explicit L1InternalSource(const daq::df::RoIBPluginInternal *config);
        ~L1InternalSource();

        virtual LVL1Result* getResult() override;
        virtual void        reset(uint32_t run_number) override;
        virtual daq::trigger::HoldTriggerInfo  hold(const std::string& dm) override;

    private:
        unsigned int          m_l1id;
        uint32_t              m_size;
        std::vector<uint32_t> m_dummy_data;
    };
}

extern "C" hltsv::L1Source *create_source(Configuration *config, const daq::df::RoIBPlugin *roib, const std::vector<std::string>& /* unused */)
{

    const daq::df::RoIBPluginInternal *my_config = config->cast<daq::df::RoIBPluginInternal>(roib);
    if(my_config == nullptr) {
        throw hltsv::ConfigFailed(ERS_HERE, "Invalid type for configuration to L1InternalSource");
    }
    return new hltsv::L1InternalSource(my_config);
}

namespace hltsv {

    L1InternalSource::L1InternalSource(const daq::df::RoIBPluginInternal *config)
        : m_l1id(0),
          m_size(config->get_FragmentSize()),
          m_dummy_data(s_ctp_template)
    {
        m_dummy_data.resize(m_size);
    }

    L1InternalSource::~L1InternalSource()
    {
    }

    LVL1Result* L1InternalSource::getResult()
    {

        if(m_hold) return nullptr;

        //create the ROB fragment
        const uint32_t lvl1_type = 0xff;

        const uint32_t event_type = 0x1; // params->getLumiBlock(); ?????

        eformat::helper::SourceIdentifier src(eformat::TDAQ_CTP, 1);

        eformat::write::ROBFragment rob(src.code(), m_run_number, m_l1id, m_l1id,
                                        lvl1_type, event_type,
                                        m_size, &m_dummy_data[0],
                                        eformat::STATUS_FRONT);

        auto fragment = new uint32_t[rob.size_word()];
        eformat::write::copy(*rob.bind(), fragment, rob.size_word());

        std::lock_guard<std::mutex> lock(m_mutex);

        uint32_t lb = m_lb;
        fragment[16] = ((lb & CTPdataformat::LumiBlockMask) << CTPdataformat::LumiBlockShift) |
            ((m_hlt_counter & CTPdataformat::HltCounterMask_v1) << CTPdataformat::HltCounterShift_v1);

        LVL1Result* l1Result = new LVL1Result(m_l1id, fragment, rob.size_word());
        m_l1id += 1;

        return l1Result;
    }


    void
    L1InternalSource::reset(uint32_t run_number)
    {
        // temporary hack to initialize L1ID
        m_l1id = 0;
        m_lb   = 1;
        m_run_number = run_number;
        m_hold = 1;
    }

    daq::trigger::HoldTriggerInfo L1InternalSource::hold(const std::string& dm)
    {
        L1Source::hold(dm);
        return { (uint8_t)((m_l1id >> 24) & 0xff), m_l1id & 0xffffff };
    }

}
