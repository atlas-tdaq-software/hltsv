/* SPDX-License-Identifier: Apache-2.0 */

#include "UnicastROSClear.h"
#include "ROSSession.h"
#include "ClearMessage.h"

#include "asyncmsg/NameService.h"
#include "asyncmsg/Message.h"                    // for OutputMessage
#include "asyncmsg/Session.h"                    // for Session, Session::State
#include "asyncmsg/detail/Header.h"              // for asyncmsg

#include "ers/Issue.h"                           // for ERS_IS_EMPTY_DEF_ERS...
#include "ers/ers.h"                             // for ERS_LOG, ERS_DEBUG

#include <cstdint>                               // for uint32_t
#include <ctime>                                 // for nanosleep
#include <ostream>                               // for operator<<, basic_os...
#include <utility>                               // for move


namespace hltsv {

    UnicastROSClear::UnicastROSClear(size_t threshold,
                                     boost::asio::io_context& service,
                                     daq::asyncmsg::NameService& name_service)
        : ROSClear(threshold),
          m_service(service),
          m_name_service(name_service)
    {
    }

    UnicastROSClear::~UnicastROSClear()
    {
        // must do this here, while object still exists.
        flush();
    }

    void UnicastROSClear::connect()
    {
        auto endpoints = m_name_service.lookup("CLEAR");

        ERS_LOG("Found " << endpoints.size() << " ROSes");

        // ERS_ASSERT(m_endpoints.size() != 0)  ? or just warning ?

        for(size_t i = 0; i < endpoints.size(); i++) {
            m_sessions.push_back(std::make_shared<ROSSession>(m_service));
        }

        for(size_t i = 0; i < endpoints.size(); i++) {
            ERS_LOG("Connection ROSSession to " << endpoints[i]);
            m_sessions[i]->asyncOpen("HLTSV", endpoints[i]);
        }

        for(auto& session : m_sessions) {
            while(session->state() != daq::asyncmsg::Session::State::OPEN) {
                timespec wait{0, 10000000};
                nanosleep(&wait, nullptr);
            }
        }

    }

    void UnicastROSClear::do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events)
    {
        using namespace daq::asyncmsg;

        ERS_DEBUG(1,"Flushing " << events->size() << " L1 IDs");

        for(auto session : m_sessions) {
            std::unique_ptr<const OutputMessage> msg(new ClearMessage(sequence,events));
            session->asyncSend(std::move(msg));
        }
    }

}
