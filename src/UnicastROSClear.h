/* SPDX-License-Identifier: Apache-2.0 */
// this is -*- C++ -*-
#ifndef HLTSV_UNICASTROSCLEAR_H_
#define HLTSV_UNICASTROSCLEAR_H_

#include "ROSClear.h"

#include <boost/asio/io_context.hpp>  // for io_context
#include "asyncmsg/NameService.h"     // for NameService

#include <cstdint>                   // for uint32_t
#include <memory>                     // for shared_ptr
#include <vector>                     // for vector

namespace hltsv {

    class ROSSession;

    /**
     * \brief Unicast implementation of ROSClear, using TCP.
     */
    class UnicastROSClear : public ROSClear {
    public:
        UnicastROSClear(size_t threshold, boost::asio::io_context& service, daq::asyncmsg::NameService& name_service);
        ~UnicastROSClear();

        virtual void connect() override;

    private:
        /// The implementation of the flush operation.

        virtual void do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events) override;
    private:
        boost::asio::io_context&                     m_service;
        daq::asyncmsg::NameService                   m_name_service;
        std::vector<std::shared_ptr<ROSSession>>     m_sessions;
    };
}

#endif // HLTSV_UNICASTROSCLEAR_H_
