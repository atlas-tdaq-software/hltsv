/* SPDX-License-Identifier: Apache-2.0 */
// this -*- c++ -*-
#ifndef HLTSV_ACTIVITY_H_
#define HLTSV_ACTIVITY_H_

// Run control, needed for inheritance
#include "RunControl/Common/Controllable.h"

// Monitoring, need template
#include "monsvc/ptr.h"

// for dynamic loading of plugins
#include "boost/dll.hpp"

#include <atomic>
#include <vector>
#include <thread>
#include <memory>
#include <string>
#include <mutex>
#include <condition_variable>

#include <boost/asio/io_context.hpp>
#include <boost/asio/executor_work_guard.hpp>

// forward declarations
namespace monsvc { class PublishingController; }
namespace daq::trigger { class CommandedTrigger; }

namespace hltsv {

  class L1Source;
  class EventScheduler;
  class ROSClear;
  class HLTSV;
  class HLTSVServer;

  /**
   * \brief Main application functionality of HLTSV.
   *
   * This class implements the run control and master trigger interface
   * and all associated actions.
   */

  class Activity : public daq::rc::Controllable {
  public:
    explicit Activity(bool restarted);
    ~Activity() noexcept;

    // Run control commands
    void configure(const daq::rc::TransitionCmd& ) override;
    void connect(const daq::rc::TransitionCmd& ) override;
    void prepareForRun(const daq::rc::TransitionCmd& ) override;
    void stopDC(const daq::rc::TransitionCmd& ) override;
    void stopRecording(const daq::rc::TransitionCmd& ) override;
    void unconfigure(const daq::rc::TransitionCmd& ) override;
    void disconnect(const daq::rc::TransitionCmd& ) override;
    void user(const daq::rc::UserCmd& cmd) override;
    void onExit(daq::rc::FSM_STATE state) noexcept override;

  private:

    // typedef for work guard in boost >= 1.87
    using work = boost::asio::executor_work_guard<boost::asio::io_context::executor_type>;

    // The internal boost::asio::io_context::run() thread.
    void io_thread(boost::asio::io_context& service);

    // The internal thread for reading the RoIB
    void l1_thread();

    // Callback for monsvc to update HLTSV object.
    void update_monitoring(const std::string& name, HLTSV *info);

    // To delay events in SV
    std::atomic<unsigned int>                                   m_event_delay;

    // To keep the io_service running.
    std::unique_ptr<std::vector<work>> m_work;
    std::unique_ptr<work>              m_ros_work;

    // io_service for DCM communication.
    std::unique_ptr<std::vector<boost::asio::io_context>>       m_io_services;

    // io_service for ROS communication
    boost::asio::io_context        m_ros_io_service;

    // HLTSVServer accepting incoming connections from the DCM
    std::shared_ptr<HLTSVServer>   m_myServer;

    // Thread to read RoIB
    std::thread                    m_l1_thread;

    // Thread pool to handle DCM communication.
    std::vector<std::thread>       m_io_threads;

    // Event Scheduler
    std::shared_ptr<EventScheduler> m_event_sched;

    // L1 Source
    std::vector<boost::dll::shared_library>  m_l1source_libs;
    std::unique_ptr<hltsv::L1Source>         m_l1source;

    // ROS Clear interface
    std::shared_ptr<ROSClear>       m_ros_clear;

    // Monitoring
    std::unique_ptr<monsvc::PublishingController> m_publisher;

    // Running flags

    // The network is active.
    bool                            m_network;

    // We are in running state.
    bool                            m_running;

    // seconds to wait at stopDC transition
    unsigned int                    m_stop_timeout;
    bool                            m_finished;
    std::mutex                      m_finished_mutex;
    std::condition_variable         m_finished_cond;

    // for MasterTrigger interface
    struct Deleter {
      void operator()(daq::trigger::CommandedTrigger *);
    };
    std::unique_ptr<daq::trigger::CommandedTrigger, Deleter> m_cmdReceiver;

    // This application has been restarted during a run.
    bool                            m_restarted;

    // The initial event ID, usually 0 except in case of restart
    uint64_t                        m_initial_event_id;

    // Maximum number of events (== 0 means no limit)
    // Note: IS RunParams is a 32bit number only.
    uint64_t                        m_max_events;

    // The HLTSV statistics and counters.
    monsvc::ptr<HLTSV>              m_stats;
  };
}

#endif // HLTSV_ACTIVITY_H_

