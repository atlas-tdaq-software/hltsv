/* SPDX-License-Identifier: Apache-2.0 */
#include "Activity.h"                            // for Activity

#include "RunControl/Common/CmdLineParser.h"     // for CmdLineParser
#include "RunControl/ItemCtrl/ItemCtrl.h"        // for ItemCtrl
#include "ipc/core.h"                            // for IPCCore
#include "ers/Issue.h"                           // for ERS_IS_EMPTY_DEF_ERS...
#include "ers/ers.h"                             // for fatal, ERS_LOG

#include <stdlib.h>                              // for exit, EXIT_FAILURE
#include <string.h>                              // for strcmp
#include <memory>                                // for make_shared, shared_ptr

int main(int argc, char** argv)
{
    try {
        bool restarted = false;

        for(int i = 1; i < argc; i++) {
            if(strcmp(argv[i], "--restart") == 0) {
                restarted = true;
                ERS_LOG("--restarted");
            }
        }

        IPCCore::init(argc,argv);
        daq::rc::CmdLineParser cmdline(argc, argv);

        daq::rc::ItemCtrl control(cmdline, std::make_shared<hltsv::Activity>(restarted));
        control.init();
        control.run();

    } catch(ers::Issue& ex) {
        ers::fatal(ex);
        exit(EXIT_FAILURE);
    }
}
