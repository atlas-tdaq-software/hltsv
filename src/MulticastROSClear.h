/* SPDX-License-Identifier: Apache-2.0 */
// this is -*- C++ -*-
#ifndef HLTSV_MULTICASTROSCLEAR_H_
#define HLTSV_MULTICASTROSCLEAR_H_

#include "ROSClear.h"

#include <stddef.h>                       // for size_t
#include <stdint.h>                       // for uint32_t
#include <boost/asio/deadline_timer.hpp>  // for deadline_timer
#include <boost/asio/io_context.hpp>      // for io_context
#include <memory>                         // for shared_ptr
#include <string>                         // for string
#include <vector>                         // for vector
namespace boost { namespace system { class error_code; } }

namespace hltsv {

    // Forward declaration for multicast session (only defined internally).
    class MCSession;

    /**
     * \brief Multicast implementation of ROSClear.
     */
    class MulticastROSClear : public ROSClear {
    public:
        MulticastROSClear(size_t threshold, boost::asio::io_context& service, const std::string& multicast_address, const std::string& outgoing);
        ~MulticastROSClear();

      virtual void connect() override;

    private:

        /// The implementation of the flush operation.
        virtual void do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events) override;

        /// Periodically sends a UDP ping
        void do_ping(const boost::system::error_code& e);

        /// The multicast session.
        std::shared_ptr<MCSession> m_session;

        /// Timer for UDP "keepalive" messages
        boost::asio::deadline_timer m_timer;
    };

}

#endif // HLTSV_MULTICASTROSCLEAR_H_
