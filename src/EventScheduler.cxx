/* SPDX-License-Identifier: Apache-2.0 */
#include "EventScheduler.h"
#include "HLTSV.h"
#include "DCMSession.h"
#include "LVL1Result.h"

#include <algorithm>
#include <random>
#include <chrono>                  // for seconds, duration
#include <iosfwd>                  // for std
#include <mutex>                   // for lock_guard
#include <vector>                  // for vector

namespace hltsv {

    EventScheduler::EventScheduler(monsvc::ptr<HLTSV> stats) :
        m_free_cores(),
        m_stats(stats),
        m_update(true),
        m_rate_thread(&EventScheduler::update_instantaneous_rate, this)
    {
        m_global_id = 0;
    }


    EventScheduler::~EventScheduler()
    {
        m_free_cores.abort();
        m_reassigned_events.abort();

        m_update = false;
        m_rate_thread.join();

    }

    // Add a DCM that has additional 'count' available cores.
    void EventScheduler::request_events(std::shared_ptr<DCMSession> dcm, unsigned int count, unsigned int finished_events)
    {
        while(count-- > 0) {
            m_free_cores.push(dcm);
        }

        {
            std::lock_guard<monsvc::ptr<HLTSV> > lock(m_stats);
            auto hltsv = m_stats.get();
            hltsv->ProcessedEvents += finished_events;

            auto numcores = m_free_cores.size();

            // total count of available cores - crude way to get it
            if( m_maxAvailable < numcores) {
                m_maxAvailable = numcores;
            }
        }
    }


    // Schedule an event to one of the available cores, after handling
    // the re-assigned events.
    void EventScheduler::schedule_event(std::shared_ptr<LVL1Result> rois)
    {
        // There is only one input thread that calls this method, so
        // we don't protect the counter updates.
        auto hltsv = m_stats.get();
        hltsv->LVL1Events++;

        auto global_id = m_global_id++;
        rois->set_global_id(global_id);

        hltsv->Recent_Global_ID = global_id;
        hltsv->Recent_LVL1_ID   = rois->l1_id();

        // First try to work on the re-assigned events if there are any
        push_events();

        std::shared_ptr<DCMSession> real_dcm;
        // now handle the new event
        do {

            m_nFrac++;
            auto n = m_free_cores.size();
            m_sumAvailable += (n > 0) ? n : 0;
            if( n <= 0 ) {
                m_nBlock++;
            }

            std::weak_ptr<DCMSession>   dcm;

            // might block
            m_free_cores.pop(dcm);

            // Try to turn weak into shared_ptr
            real_dcm = dcm.lock();
            if(real_dcm) {
                if(!real_dcm->handle_event(rois)) {
                    real_dcm.reset();
                    continue;
                }
            }
        } while(!real_dcm);

        hltsv->AssignedEvents++;
    }

    void EventScheduler::push_events()
    {
        std::weak_ptr<DCMSession>   dcm;
        std::shared_ptr<DCMSession> real_dcm;

        std::shared_ptr<LVL1Result> revent;

        unsigned int events = 0;
        while(m_reassigned_events.try_pop(revent)) {
            do {
                // might block
                m_free_cores.pop(dcm);
                real_dcm = dcm.lock();
                if(real_dcm) {
                    if(!real_dcm->handle_event(revent)) {
                        real_dcm.reset();
                        continue;
                    }
                    events++;
                }
            } while(!real_dcm);
        }

        if(events > 0) {
            auto hltsv = m_stats.get();
            hltsv->ReassignedEvents += events;;
        }

    }

    // Re-assign an event if there was a problem with the DCM.
    void EventScheduler::reassign_event(std::shared_ptr<LVL1Result> rois)
    {
        rois->set_reassigned();
        m_reassigned_events.push(rois);
    }

    void EventScheduler::reset(uint64_t initial_event_id)
    {
        m_global_id = initial_event_id;
        m_stats->reset();
        m_nFrac        = 0;
        m_nBlock       = 0;
        m_sumAvailable = 0;

        {
            decltype(m_free_cores)::value_type elem;
            while(m_free_cores.try_pop(elem)) {}
        }

        {
            decltype(m_reassigned_events)::value_type elem;
            while(m_reassigned_events.try_pop(elem)) {}
        }
    }

    void EventScheduler::shuffle()
    {
        using namespace std;

        vector<weak_ptr<DCMSession>>  temp;
        temp.reserve(m_free_cores.size());

        weak_ptr<DCMSession> dcm;
        while(m_free_cores.try_pop(dcm)) {
            temp.push_back(dcm);
        }

        std::random_device rd;
        std::mt19937 urg(rd());

        std::shuffle(temp.begin(), temp.end(), urg);

        for(auto& d : temp) {
            m_free_cores.push(d);
        }
    }

    void EventScheduler::update_instantaneous_rate()
    {
        uint64_t last_count = m_stats->ProcessedEvents;

        const auto sleep_interval = std::chrono::seconds(1);

        while(m_update) {
            std::this_thread::sleep_for(sleep_interval);

            auto n = m_free_cores.size();

            std::lock_guard<monsvc::ptr<HLTSV> > lock(m_stats);
            auto hltsv = m_stats.get();

            hltsv->AvailableCores = n < 0 ? 0 : n;

            hltsv->MaxAvailable = m_maxAvailable;
            if(m_nFrac > 0) {
                hltsv->FracAvailable = (m_maxAvailable > 0) ?  m_sumAvailable/(float)(m_nFrac * m_maxAvailable) : 1.;
                hltsv->Busy          = (float)100. * (float)m_nBlock / (float)m_nFrac;
            } else {
                hltsv->FracAvailable = 1.0;
                hltsv->Busy          = 0.0;
            }
            m_sumAvailable = 0;
            m_nFrac        = 0;
            m_nBlock       = 0;
            m_maxAvailable = 0;

            if(hltsv->ProcessedEvents >= last_count)  {
                hltsv->Rate = (double)(hltsv->ProcessedEvents - last_count)/(double )sleep_interval.count();
            }

            last_count    = hltsv->ProcessedEvents;
        }
    }

    void EventScheduler::abort()
    {
        m_free_cores.abort();
    }
}
